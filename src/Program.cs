﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Options;

namespace SimulatorDataConvertor
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputFile = string.Empty;
            string outputFile = "output.xml";
            bool showHelp = false;

            OptionSet p = new OptionSet()
                .Add("i:", "Input file name", delegate (string v) { if (v != null) inputFile = v; })
                .Add("o:", "Output file name, (default: output.xml)", delegate (string v) { if (v != null) outputFile = v; })
                .Add("h|?|help", "Print this help information.", delegate (string v) { showHelp = v != null; });

            if (args.Length == 0)
            {
                ShowHelp(p);
                return;
            }

            List<string> extra;
            try
            {
                extra = p.Parse(args);
            }
            catch (OptionException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            if (showHelp)
            {
                ShowHelp(p);
                return;
            }

            if (string.IsNullOrEmpty(inputFile))
            {
                Console.WriteLine("Input file name need to be specified.");
                return;
            }

            var iReasoningData = IReasoningSnmpSimulatorDataSerializer.ReadFile(inputFile);

            var model = string.Empty;
            var xianSnmpData = new Snmpdevice
            {
                Company = "Custom",
                Category = "Customer Devices",
                Guid = Guid.NewGuid().ToString(),
                Version = "1.0.5.0"
            };
            xianSnmpData.Snmpvar = new List<Snmpvar>();

            foreach (var item in iReasoningData.Instances.Instance)
            {
                if(item.Oid == ".1.3.6.1.2.1.1.5.0")
                {
                    model = item.Value.Text;
                }

                xianSnmpData.Snmpvar.Add(new Snmpvar
                {
                    Oid = item.Oid.Substring(1),
                    Syntax = SyntaxConvert(item.ValueType),
                    Record = new Record { Value = ValueConvert(item.ValueType, item.Value.Text) }
                });
            }

            xianSnmpData.Model = string.IsNullOrWhiteSpace(model) ? "EmptyModel" : model;

            XianSnmpSimulatorDataSerializer.WriteFile(outputFile, xianSnmpData);
        }

        static string SyntaxConvert(string origin)
        {
            if (origin == "OID")
                return "OBJECTIDENTIFIER";
            if (origin == "Counter32")
                return "COUNTER";
            return origin.ToUpper();
        }

        static string ValueConvert(string syntax, string orign)
        {
            if (syntax == "OctetString")
            {
                if (orign.IndexOf("0x") > -1) return orign.Replace("0x", "");
                return ToHexString(Encoding.UTF8.GetBytes(orign));
            }
            if (syntax == "TimeTicks")
                return "7190";
            if (syntax == "OID")
                return orign.Substring(1);
            return orign;
        }

        static string ToHexString(byte[] data)
        {
            StringBuilder b = new StringBuilder();
            for (int i = 0; i < data.Length; ++i)
            {
                int x = (int)data[i] & 0xff;
                if (x < 16)
                    b.Append('0');
                b.Append(System.Convert.ToString(x, 16).ToUpper());

                if (i < data.Length - 1)
                    b.Append(' ');
            }
            return b.ToString();
        }

        private static void ShowHelp(OptionSet optionSet)
        {
            Console.WriteLine("SimulatorDataConvertor.exe [Options]");
            Console.WriteLine("Options:");
            optionSet.WriteOptionDescriptions(Console.Out);
        }
    }
}
