﻿/* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace SimulatorDataConvertor
{
    [XmlRoot(ElementName = "Instance")]
    public class Instance
    {
        [XmlAttribute(AttributeName = "oid")]
        public string Oid { get; set; }
        [XmlAttribute(AttributeName = "valueType")]
        public string ValueType { get; set; }
        [XmlElement(ElementName = "Value")]
        public Value Value { get; set; }
    }

    [XmlRoot(ElementName = "Value")]
    public class Value
    {
        [XmlAttribute(AttributeName = "isSimpleScript")]
        public string IsSimpleScript { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Instances")]
    public class Instances
    {
        [XmlElement(ElementName = "Instance")]
        public List<Instance> Instance { get; set; }
        [XmlAttribute(AttributeName = "readCommunity")]
        public string ReadCommunity { get; set; }
    }

    [XmlRoot(ElementName = "SnmpSimulatorData")]
    public class SnmpSimulatorData
    {
        [XmlElement(ElementName = "Instances")]
        public Instances Instances { get; set; }
    }

}

