﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SimulatorDataConvertor
{
    class IReasoningSnmpSimulatorDataSerializer
    {
        private XmlSerializer s = null;
        private Type type = null;

        /// <summary>Default constructor.</summary>
        public IReasoningSnmpSimulatorDataSerializer()
        {
            this.type = typeof(SnmpSimulatorData);
            this.s = new XmlSerializer(this.type);
        }

        public SnmpSimulatorData Deserialize(string xml)
        {
            TextReader reader = new StringReader(xml);
            return Deserialize(reader);
        }

        public SnmpSimulatorData Deserialize(XmlDocument doc)
        {
            TextReader reader = new StringReader(doc.OuterXml);
            return Deserialize(reader);
        }


        public SnmpSimulatorData Deserialize(TextReader reader)
        {
            SnmpSimulatorData o = (SnmpSimulatorData)s.Deserialize(reader);
            reader.Close();
            return o;
        }
        
        public XmlDocument Serialize(SnmpSimulatorData data)
        {
            string xml = StringSerialize(data);
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.LoadXml(xml);
            doc = Clean(doc);
            return doc;
        }

        private string StringSerialize(SnmpSimulatorData data)
        {
            TextWriter w = WriterSerialize(data);
            string xml = w.ToString();
            w.Close();
            return xml;
        }

        private TextWriter WriterSerialize(SnmpSimulatorData data)
        {
            TextWriter w = new StringWriter();
            this.s = new XmlSerializer(this.type);
            s.Serialize(w, data);
            w.Flush();
            return w;
        }

        private XmlDocument Clean(XmlDocument doc)
        {
            doc.RemoveChild(doc.FirstChild);
            XmlNode first = doc.FirstChild;
            foreach (XmlNode n in doc.ChildNodes)
            {
                if (n.NodeType == XmlNodeType.Element)
                {
                    first = n;
                    break;
                }
            }
            if (first.Attributes != null)
            {
                XmlAttribute a = null;
                a = first.Attributes["xmlns:xsd"];
                if (a != null) { first.Attributes.Remove(a); }
                a = first.Attributes["xmlns:xsi"];
                if (a != null) { first.Attributes.Remove(a); }
            }
            return doc;
        }

        /// <summary>Reads config data from config file.</summary>
        /// <param name="file">Config file name.</param>
        /// <returns></returns>
        public static SnmpSimulatorData ReadFile(string file)
        {
            IReasoningSnmpSimulatorDataSerializer serializer = new IReasoningSnmpSimulatorDataSerializer();
            try
            {
                string xml = string.Empty;
                using (StreamReader reader = new StreamReader(file))
                {
                    xml = reader.ReadToEnd();
                    reader.Close();
                }
                return serializer.Deserialize(xml);
            }
            catch { }
            return new SnmpSimulatorData();
        }

        /// <summary>Writes config data to config file.</summary>
        /// <param name="file">Config file name.</param>
        /// <param name="config">Config object.</param>
        /// <returns></returns>
        public static bool WriteFile(string file, SnmpSimulatorData config)
        {
            bool ok = false;
            var serializer = new IReasoningSnmpSimulatorDataSerializer();
            try
            {
                string xml = serializer.Serialize(config).OuterXml;
                using (StreamWriter writer = new StreamWriter(file, false))
                {
                    writer.Write(xml);
                    writer.Flush();
                    writer.Close();
                }
                ok = true;
            }
            catch { }
            return ok;
        }
    }
}
