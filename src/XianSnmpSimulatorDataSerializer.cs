﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SimulatorDataConvertor
{
    class XianSnmpSimulatorDataSerializer
    {
        private XmlSerializer s = null;
        private Type type = null;

        /// <summary>Default constructor.</summary>
        public XianSnmpSimulatorDataSerializer()
        {
            this.type = typeof(Snmpdevice);
            this.s = new XmlSerializer(this.type);
        }

        public Snmpdevice Deserialize(string xml)
        {
            TextReader reader = new StringReader(xml);
            return Deserialize(reader);
        }

        public Snmpdevice Deserialize(XmlDocument doc)
        {
            TextReader reader = new StringReader(doc.OuterXml);
            return Deserialize(reader);
        }


        public Snmpdevice Deserialize(TextReader reader)
        {
            Snmpdevice o = (Snmpdevice)s.Deserialize(reader);
            reader.Close();
            return o;
        }

        public XmlDocument Serialize(Snmpdevice data)
        {
            string xml = StringSerialize(data);
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.LoadXml(xml);
            doc = Clean(doc);
            return doc;
        }

        private string StringSerialize(Snmpdevice data)
        {
            TextWriter w = WriterSerialize(data);
            string xml = w.ToString();
            w.Close();
            return xml;
        }

        private TextWriter WriterSerialize(Snmpdevice data)
        {
            TextWriter w = new StringWriter();
            this.s = new XmlSerializer(this.type);
            s.Serialize(w, data);
            w.Flush();
            return w;
        }

        private XmlDocument Clean(XmlDocument doc)
        {
            doc.RemoveChild(doc.FirstChild);
            XmlNode first = doc.FirstChild;
            foreach (XmlNode n in doc.ChildNodes)
            {
                if (n.NodeType == XmlNodeType.Element)
                {
                    first = n;
                    break;
                }
            }
            if (first.Attributes != null)
            {
                XmlAttribute a = null;
                a = first.Attributes["xmlns:xsd"];
                if (a != null) { first.Attributes.Remove(a); }
                a = first.Attributes["xmlns:xsi"];
                if (a != null) { first.Attributes.Remove(a); }
            }
            return doc;
        }

        /// <summary>Reads config data from config file.</summary>
        /// <param name="file">Config file name.</param>
        /// <returns></returns>
        public static Snmpdevice ReadFile(string file)
        {
            var serializer = new XianSnmpSimulatorDataSerializer();
            try
            {
                string xml = string.Empty;
                using (StreamReader reader = new StreamReader(file))
                {
                    xml = reader.ReadToEnd();
                    reader.Close();
                }
                return serializer.Deserialize(xml);
            }
            catch { }
            return new Snmpdevice();
        }

        /// <summary>Writes config data to config file.</summary>
        /// <param name="file">Config file name.</param>
        /// <param name="config">Config object.</param>
        /// <returns></returns>
        public static bool WriteFile(string file, Snmpdevice config)
        {
            bool ok = false;
            var serializer = new XianSnmpSimulatorDataSerializer();
            try
            {
                string xml = serializer.Serialize(config).OuterXml;
                using (StreamWriter writer = new StreamWriter(file, false))
                {
                    writer.Write(xml);
                    writer.Flush();
                    writer.Close();
                }
                ok = true;
            }
            catch { }
            return ok;
        }
    }
}
