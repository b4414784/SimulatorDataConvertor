﻿/* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace SimulatorDataConvertor
{
    [XmlRoot(ElementName = "record")]
    public class Record
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "snmpvar")]
    public class Snmpvar
    {
        [XmlElement(ElementName = "record")]
        public Record Record { get; set; }
        [XmlAttribute(AttributeName = "oid")]
        public string Oid { get; set; }
        [XmlAttribute(AttributeName = "syntax")]
        public string Syntax { get; set; }
    }

    [XmlRoot(ElementName = "snmpdevice")]
    public class Snmpdevice
    {
        [XmlElement(ElementName = "snmpvar")]
        public List<Snmpvar> Snmpvar { get; set; }
        [XmlAttribute(AttributeName = "company")]
        public string Company { get; set; }
        [XmlAttribute(AttributeName = "category")]
        public string Category { get; set; }
        [XmlAttribute(AttributeName = "model")]
        public string Model { get; set; }
        [XmlAttribute(AttributeName = "guid")]
        public string Guid { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
    }

}
