# SimulatorDataConvertor

#### 项目介绍
将IReasoning SNMP Agent Data Record结果转换为Xian SNMP Simulator数据格式

### 参考
IReasoning SNMP Simulator [http://ireasoning.com/snmpsimulator.shtml](http://ireasoning.com/snmpsimulator.shtml)

Xian SNMP Simulator [http://www.jalasoft.com/xian/snmpsimulator](http://www.jalasoft.com/xian/snmpsimulator)
 * 本人已破解该软件


#### 使用说明

```
SimulatorDataConvertor.exe [Options]
Options:
  -i[=VALUE]                 Input file name
  -o[=VALUE]                 Output file name, (default: output.xml)
  -h, -?, --help             Print this help information.
```